const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
    describe("RGB to Hex conversion", () => {
        it("converts the basic colors", () => {
            const redHex = converter.rgbToHex(255,0,0); // ff0000
            const greenHex = converter.rgbToHex(0,255,0); // 00ff00
            const blueHex = converter.rgbToHex(0,0,255); // 0000ff

            expect(redHex).to.equal("ff0000");
            expect(greenHex).to.equal("00ff00");
            expect(blueHex).to.equal("0000ff");
        });
    });
    describe("Hex to RGB conversion", () => {
        it("converts the basic colors", () => {
            const redRGB = converter.hexToRgb("ff0000"); // ff0000
            const greenRGB = converter.hexToRgb("00ff00"); // 00ff00
            const blueRGB = converter.hexToRgb("0000ff"); // 0000ff
            const RGBs = [redRGB,greenRGB,blueRGB]
            for(let i=0; i!=3; i++){
                expect(RGBs[i][0]).to.equal(i==0?255:0);
                expect(RGBs[i][1]).to.equal(i==1?255:0);
                expect(RGBs[i][2]).to.equal(i==2?255:0);
            }
        });
    });
});
